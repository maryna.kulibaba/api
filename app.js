const filesFolder = `${__dirname}/files/`;
const settingsFilePath = './settings.json';
const logFilePath = './logs.txt';
const http = require('http');
const fs = require('fs');
const url = require('url');

const settings = readSettings();
if(!fs.existsSync(filesFolder)) { 
    fs.mkdirSync(filesFolder);
}

http.createServer((req, res) => {
    try {
        writelog(`${req.method}, ${req.url}`);

        if (/^\/?api\/files$/.test(req.url)) {
            if (req.method === 'POST') {
                createFile(req, res);
            } else if (req.method === 'PUT') {
                createFile(req, res, true);
            } else if (req.method === 'GET') {
                getFilesList(req, res);
            } else {
                throw new InternalServerError();
            }
        } else if (/^\/?api\/files\/.*\??$/.test(req.url)) {
            const _url = new URL('http://server' + req.url);
            const password = _url.searchParams.get('password');
            let fileName = _url.pathname.replace(/^\/?api\/files\//, "");
            if (req.method === "GET") {
                getFile(fileName, password, res);
            } else if (req.method === 'DELETE') {
                deleteFile(fileName, password, res);
            } else {
                throw new InternalServerError();
            }
        } else {
            throw new InternalServerError();
        }
    } catch(error) {
        writelog(error.message, 'ERR');
        writeResponse(res, error);
    }
})
.on('error', function (error) {
    console.error(error, error.stack);
    writelog(error.message, 'ERR');
})
.listen(8080, () => {
    console.log('server start 8080');
});
process.on('uncaughtException', function( error) {
    console.error(error, error.stack);
    writelog(error.message, 'FAT');
});

const createFile = (req, res, override = false) => {
    writelog('createFile');
    let body = "";
    req.on('data', function (data) {
        body += data;
        // Too much POST data, kill the connection!
        // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
        if (body.length > 1e6) {
            req.connection.destroy();
            writeResponse(res, new InternalServerError());
        }
    });
    req.on('end', function () {
        try {
            let data = JSON.parse(body || "{}");
            if (typeof (data.filename) === 'undefined' || !data.filename || data.filename.length === 0) {
                throw new BadRequestError("Please specify 'filename' parameter");
            }
            if (typeof (data.content) === 'undefined' || !data.content || data.content.length === 0) {
                throw new BadRequestError("Please specify 'content' parameter");
            }
            let filename = data.filename;
            if (['log', 'txt', 'json', 'yaml', 'xml', 'js'].indexOf(getFileExtension(filename)) === -1) {
                throw new BadRequestError("Server supports 'log', 'txt', 'json', 'yaml', 'xml', 'js' files");
            }
            let pathToFile = `${filesFolder}${filename}`;
            if(fs.existsSync(pathToFile) && !override) {
                throw new BadRequestError(`Impossible create '${filename}' file, file already exists`);
            }
            if (typeof (data.password) !== 'undefined' && data.password) {
                if (data.password.length === 0) {
                    throw new BadRequestError("Password can't be empty");
                }
                if (override) {
                    throw new BadRequestError("You can't change password");
                }
            }
            let options = { encoding: 'utf8' };
            if (override) {
                options.flag = 'w';
            }
            fs.writeFile(pathToFile, data.content, options, function (err) {
                if (err) {
                    console.error(err);
                    writelog(err.message, 'ERR');
                    writeResponse(res, new InternalServerError());
                    return;
                }
                writelog(`File '${filename}' created successfully`);
                if (typeof (data.password) !== 'undefined' && data.password && data.password.length > 0 && !override) {
                    settings.protection[filename] = data.password;
                    writeSettings();
                    writelog(`File '${filename}' protected`);
                }
                writeResponse(res, {message: "File created successfully"});
            });
        } catch (error) {
            writelog(error.message, 'ERR');
            writeResponse(res, error);
        }
    });
    req.on('error',  (error) => {
        writelog(error.message, 'ERR');
    })
}

const getFilesList = (req, res) => {
    // ????? writeResponse(res, new BadRequestError("Client error"));
    writelog('getFilesList');
    if(!fs.existsSync(filesFolder)) {
        writelog(`Folder ${filesFolder} not found`, 'ERR');
        writeResponse(res, new InternalServerError());
        return;
    }
    fs.readdir(filesFolder, (err, files) => {
        let obj = err ? new InternalServerError() :  { "message": "Success", "files": files };
        writelog(err ? err.message : JSON.stringify(obj), err ? 'ERR' : 'INF');
        writeResponse(res, obj);
    });
}

const getFile = (filename, password, res) => {
    writelog(`getFile '${filename}'`);
    let pathToFile = `${filesFolder}${filename}`;
//    try {
        if (filename.length === 0) {
            throw new BadRequestError(`No file filename found`);
        }
        if (settings.protection[filename]) {
            if (!password || password.length === 0) {
                throw new new BadRequestError(`Impossible access to protected resource`);
            }
            if (settings.protection[filename] !== password) {
                throw new BadRequestError(`Wrong password`);
            }
        }
        if(!fs.existsSync(pathToFile)) {
            throw new BadRequestError(`No file with '${filename}' filename found`)
        }
        fs.readFile(pathToFile, (err, data) => {
            const { ctime } = fs.statSync(pathToFile);
            let obj = err ? new InternalServerError() :  {
                "message": "Success",
                "filename": filename,
                "content": data.toString(),
                "extension": getFileExtension(filename),
                "uploadedDate": ctime.toISOString()
            };
            writelog(err ? err.message : JSON.stringify(obj), err ? 'ERR' : 'INF');
            writeResponse(res, obj);
        });
//    } catch(err) {
//        writelog(err.message, 'ERR');
//        writeResponse(res, err);
//    }
}

const deleteFile = (filename, password, res) => {
    writelog(`deleteFile '${filename}'`);
    let pathToFile = `${filesFolder}${filename}`;
    if (filename.length === 0) {
        writeResponse(res, new BadRequestError(`No file filename found`));
        return;
    }
    if (settings.protection[filename]) {
        if (!password || password.length === 0) {
            writeResponse(res, new BadRequestError(`Impossible access to protected resource`));
            return;
        }
        if (settings.protection[filename] !== password) {
            writeResponse(res, new BadRequestError(`Wrong password`));
            return;
        }
    }
    if(fs.existsSync(pathToFile)) {
        fs.unlink(pathToFile, (err, data) => {
            let obj = err ? new InternalServerError() : { "message": `File '${filename}' deleted successfully` };
            writeResponse(res, obj);
        });
    } else {
        writeResponse(res, new BadRequestError(`No file with '${filename}' filename found`));
    }
}

const getFileExtension = (filename) => {
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename)[0] : '';
}

const writeResponse = (res, obj) => {
    // todo check obj instanceof Error
    // todo check obj.name is numeric
    res.writeHead( obj.name || "200", {'content-type' : 'application/json'});
    res.end( JSON.stringify(obj instanceof Error ? { message: obj.message } : obj));
}

function writeSettings() {
    const data = JSON.stringify(settings);
    fs.writeFileSync(settingsFilePath, data);
}

function readSettings() {
    try {
        const data = fs.existsSync(settingsFilePath) ? fs.readFileSync(settingsFilePath) : "{ \"protection\": {} }";
        return JSON.parse(data || "{ \"protection\": {} }");
    } catch (err) {
        console.log(err);
    }
}

function writelog(message, level = 'INF') {
    let data = `${new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')}   [${level}]  ${message}` + "\r\n";
    fs.appendFile(logFilePath, data, () => {});
}

// =====================================================

function BadRequestError(message) {
    this.name = '400';
    this.message = message || 'Bad request';
    this.stack = (new Error()).stack;
}
BadRequestError.prototype = Object.create(Error.prototype);
BadRequestError.prototype.constructor = BadRequestError;

function InternalServerError(message) {
    this.name = '500';
    this.message = message || 'Server error';
    this.stack = (new Error()).stack;
}
InternalServerError.prototype = Object.create(Error.prototype);
InternalServerError.prototype.constructor = InternalServerError;
